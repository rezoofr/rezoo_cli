<?php

namespace Rezoo;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Configuration
 *
 * @author Jérémy LEHERPEUR
 */
class Configuration implements CompilerPassInterface
{
    private $container;

    public function __construct($configPath)
    {
        $this->container = new ContainerBuilder();
        $this->container->addCompilerPass($this);

        $loader = new YamlFileLoader($this->container, new FileLocator($configPath));
        $loader->load('services.yml');

        $loader = new YamlFileLoader($this->container, new FileLocator(__DIR__."/config"));
        $loader->load('services.yml');

        $this->container->compile();
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function process(ContainerBuilder $container)
    {
        $roles = $this->container->findTaggedServiceIds("rezoo.cli.role");

        $console_def = $container->getDefinition(
            'console'
        );

        foreach ($roles as $id => $attributes) {
            $console_def->addMethodCall('addRole', array(
                new Reference($id)
            ));
        }
    }
}