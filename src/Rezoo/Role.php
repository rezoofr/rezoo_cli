<?php

namespace Rezoo;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Finder\Finder;

abstract class Role implements RoleInterface
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerBuilder
     */
    protected $container;

    protected $reflected;

    public function __construct(ContainerBuilder $container)
    {
        $this->container = $container;
    }

    public function boot(Application $console)
    {
        $this->registerCommands($console);
    }

    /**
     * Gets the ReflectionObject of this instance.
     *
     * @return \ReflectionObject The ReflectionObject
     *
     */
    private function getReflected()
    {
        if (null === $this->reflected) {
            $this->reflected = new \ReflectionObject($this);
        }
        return $this->reflected;
    }

    /**
     * Gets the Role namespace.
     *
     * @return string The Role namespace
     *
     */
    public function getNamespace()
    {
        return $this->getReflected()->getNamespaceName();
    }

    /**
     * Gets the Role directory path.
     *
     * @return string The Role absolute path
     *
     */
    public function getPath()
    {
        return dirname($this->getReflected()->getFileName());
    }

    /**
     * Register all Role Commands into the Application
     * @param Application $console
     */
    public function registerCommands(Application $console)
    {
        if (!is_dir($dir = $this->getPath().'/Command')) {
            return;
        }

        $finder = new Finder();
        $finder->files()->name('*Command.php')->in($dir);

        $prefix = $this->getNamespace().'\\Command';
        /** @var \Symfony\Component\Finder\SplFileInfo $file */
        foreach ($finder as $file) {
            $ns = $prefix;
            if ($relativePath = $file->getRelativePath()) {
                $ns .= '\\'.strtr($relativePath, '/', '\\');
            }
            $r = new \ReflectionClass($ns.'\\'.$file->getBasename('.php'));
            if ($r->isSubclassOf('Rezoo\\Command') && !$r->isAbstract() && $r->getConstructor()->getNumberOfRequiredParameters() == 1) {
                /** @var \Rezoo\Command $command */
                $command = $r->newInstance($this->container);
                $console->add($command);
            }
        }
    }
}