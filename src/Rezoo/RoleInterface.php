<?php

namespace Rezoo;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\DependencyInjection\ContainerBuilder;

interface RoleInterface
{
    public function __construct(ContainerBuilder $builder);
    public function boot(Application $application);
}