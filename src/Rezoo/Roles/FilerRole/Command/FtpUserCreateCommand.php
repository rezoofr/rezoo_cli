<?php

namespace Rezoo\Roles\FilerRole\Command;

use Rezoo\Command as BaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FtpUserCreateCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('ftp:user:create')
            ->setDescription('Create a FTP User')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->error("BUG !!!");
    }
}