<?php

namespace Rezoo;

use Symfony\Component\Console\Application;

class Console extends Application
{
    private $roles = array();

    public function addRole(RoleInterface $role)
    {
        $this->roles[] = $role;
        $role->boot($this);
    }
}