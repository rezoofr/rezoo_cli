<?php

namespace Rezoo;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command as BaseCommand;
use Symfony\Component\DependencyInjection\ContainerBuilder;

abstract class Command extends BaseCommand
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerBuilder
     */
    private $container;

    public function __construct(ContainerBuilder $container, $name = null)
    {
        $this->container = $container;
        parent::__construct($name);
    }

    public function writeln(OutputInterface $output, $messages, $type = OutputInterface::OUTPUT_NORMAL)
    {
        // Output
        if ($type <= $output->getVerbosity()) {
            $output->writeln($messages);
        }

        // Log
        if(!is_array($messages))
        {
            $messages = array($messages);
        }

        /** @var string $message */
        foreach($messages as $message)
        {
            switch($type)
            {
                case OutputInterface::VERBOSITY_DEBUG:
                case OutputInterface::VERBOSITY_VERY_VERBOSE:
                case OutputInterface::VERBOSITY_VERBOSE:
                    $this->container->get('logger')->debug($message);
                    break;
            }
        }
    }

    public function error($message)
    {
        $this->container->get('logger')->error($message);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Sample for output level
        $this->writeln($output, "VERBOSITY_QUIET", OutputInterface::VERBOSITY_QUIET);
        $this->writeln($output, "VERBOSITY_NORMAL", OutputInterface::VERBOSITY_NORMAL);
        $this->writeln($output, "VERBOSITY_VERBOSE", OutputInterface::VERBOSITY_VERBOSE);
        $this->writeln($output, "VERBOSITY_VERY_VERBOSE", OutputInterface::VERBOSITY_VERY_VERBOSE);
        $this->writeln($output, "VERBOSITY_DEBUG", OutputInterface::VERBOSITY_DEBUG);
    }
}